class SimpleParticle
{
  float pos[]={0,0};
  long birthTime;
  long deathTime;
  long lifeTime;
  
  //constructor
  SimpleParticle()
  {
    start();
  }
  
  void start()
  {
    birthTime = millis();
    lifeTime = (long)random(10000);//random lifetime
  }
  
  void die()
  {
  }
  
//  called every time we want to move our particle
//  checks the position against the view rectangle, 
//  life time and appplies the velocity to the position
  void update(float vel[])
  {
    pos[0]+=vel[0];
    pos[1]+=vel[1];
    
    //if we leave the viewing rect or run out of time then die
    if((pos[0] > width  || pos[0] < 0) || 
       (pos[1] > height || pos[1] < 0) || 
       millis() - birthTime > lifeTime)
    {
      die();
    }
    else
    {
      deathTime = millis();
    }
  }
  
  //draw this particle
  void draw()
  {
    noStroke();
    fill(255);
    ellipse(pos[0],pos[1],2,2);
  }
}
