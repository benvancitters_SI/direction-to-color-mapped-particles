// render mode - controls whether we see the direction 
// field or a painting in the background
int mode = 0;

ParticleSystem ps = new ParticleSystem();
PImage img;

float mouseMults[];
                    
//called when the app is started
void setup()
{
  size(1200,900);
}

//called when the mouse is pressed and then released quickly
void mouseClicked()
{
  mode = (mode +1)%6;
  if(mode == 1)
  { img = loadImage("monet.jpg"); }
  else if(mode == 2)
  { img = loadImage("renoir.jpg"); }
  else if(mode == 3)
  { img = loadImage("rousseau.jpg"); }
  else if(mode == 4)
  { img = loadImage("monet2.jpg"); }
  else if(mode == 5)
  { img = loadImage("IMG_0063.JPG"); }
}

//called about 60 times a second
void draw()
{
  //draw a custom color/direction field
  if(mode == 0)
  {
    updateColors();
    stroke(255);
    noFill();
    ellipse(width/2,height/2,70.f/(.01+mouseMults[0]),70.f/(.01+mouseMults[1]));
  }
  else
  {
    //fill the background with an image
    image(img,0,0,width,height);
  }
  ps.update();
  ps.draw();
}

//called each update tick to create the direction-field-as-color
void updateColors()
{
  loadPixels();
   mouseMults = new float[]{4*mouseX*1.f/width,
                        4*mouseY*1.f/height};
  //screen midpoint - where everything moves away from                      
  float mid[] = {width/2,height/2};
  for(int i = 0; i < pixels.length; i++)
  {
    float x = i%width;
    float y = i/width;
    float tmp[]= {x-mid[0],y-mid[1]};
    float d = dist(0,0,tmp[0]*mouseMults[0],
                       tmp[1]*mouseMults[1]);  
    
    tmp[0] *= 128.f/d;
    tmp[1] *= 128.f/d;
    tmp[0] += 128;
    tmp[1] += 128;    
    pixels[i] = color(tmp[0], //red
                      tmp[1], //green
                      0);
  }
  updatePixels();
}
