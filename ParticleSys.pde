class ParticleSystem
{
  ArrayList<SimpleParticle> parts = new ArrayList<SimpleParticle>();
  
  //constructor
  ParticleSystem()
  {
    //make 7000 particles!!!
    for(int i = 0; i < 7000; i++)
    {
      SimpleParticle sp = new SimpleParticle();
      sp.pos = new float[]{random(width),random(height)};
      parts.add(sp);
    }
  } 
 
  //draw all particles - called every tick 
  void draw()
  {
    for(SimpleParticle p : parts)
    {
      p.draw();
    }
  }
  
  //update - called every tick
  void update()
  {
    long tm = millis();
    for(SimpleParticle p : parts)
    {
      // we move a fraction of our total speed and then integrate
      // the more iterations the more processing power needed
      int iterations = 2;
      for(int i = 0; i < iterations; i++)
      {
        //extract the velocity from the color of the screen below the particle
        int c = get((int)p.pos[0],
                    (int)p.pos[1]);
        float v[] =new float[]{red(c)/255.f - .5,
                               green(c)/255.f - .5};
                               v[0] *= 10.f/iterations;
                               v[1] *= 10.f/iterations;

        p.update(v);
        if(p.deathTime < tm)
        {
          //if the particle is dead - rebirth it
          p.pos = new float[]{random(width),random(height)};
          p.start();
        }
      }
    }
  }
}
